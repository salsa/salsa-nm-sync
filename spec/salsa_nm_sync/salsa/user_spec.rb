# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Salsa::User do
  describe '#initialize' do
    it do
      o = SalsaNmSync::Salsa::User.new(
        id: 1,
        custom_attributes: {
          'nm-sync-email' => 'test@example.com',
          'nm-sync-profile' => 'https://example.com',
          'nm-sync-status' => 'debian_developer',
        },
        status: 'active',
        client: nil,
      )

      expect(o.sub).to eq('1')
      expect(o.email).to eq('test@example.com')
      expect(o.profile).to eq('https://example.com')
      expect(o.status).to eq(:debian_developer)
      expect(o.gitlab_status).to eq(:active)
    end
  end

  describe '#status=' do
    it 'no change if new status == old status' do
      client = double(SalsaNmSync::Utils::GitlabApi)

      user = SalsaNmSync::Salsa::User.new(
        id: 1,
        custom_attributes: {'nm-sync-status' => 'debian_developer'},
        status: nil,
        client: client,
      )

      user.status = :debian_developer
      expect(user.status).to be(:debian_developer)
    end

    it 'sets new status' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).not_to receive(:block_user)
      expect(client).to receive(:set_user_custom_attribute).with(1, 'nm-sync-status', 'debian_developer')

      user = SalsaNmSync::Salsa::User.new(
        id: 1,
        custom_attributes: {},
        status: nil,
        client: client,
      )

      user.status = :debian_developer
      expect(user.status).to be(:debian_developer)
    end

    it 'sets new status and blocks user' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:block_user).with(1)
      expect(client).to receive(:set_user_custom_attribute).with(1, 'nm-sync-status', 'debian_removed_dd')

      user = SalsaNmSync::Salsa::User.new(
        id: 1,
        custom_attributes: {},
        status: nil,
        client: client,
      )

      user.status = :debian_removed_dd
      expect(user.status).to be(:debian_removed_dd)
      expect(user.gitlab_status).to be(:blocked)
    end

    it 'removes status' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:delete_user_custom_attribute).with(1, 'nm-sync-status')

      user = SalsaNmSync::Salsa::User.new(
        id: 1,
        custom_attributes: {'nm-sync-status' => 'debian_developer'},
        status: nil,
        client: client,
      )

      user.status = nil
      expect(user.status).to be_nil
    end
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Salsa::UserList do
  describe '#load_remote' do
    it do
      response = double(HTTParty::Response)
      expect(response).to receive(:parsed_response).and_return([
        {
          'id' => 23,
          'state' => 'active',
          'custom_attributes' => [
            { 'key' => 'nm-sync-email', 'value' => 'email' },
            { 'key' => 'nm-sync-profile', 'value' => 'profile' },
            { 'key' => 'nm-sync-status', 'value' => 'debian_developer' },
          ],
        },
      ])

      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:get_users).and_return(response)

      o = SalsaNmSync::Salsa::UserList.new(
        client: client,
      )
      o.load_remote()

      expect(o.size).to eq(1)
      expect(o['23'].email).to eq('email')
      expect(o['23'].profile).to eq('profile')
      expect(o['23'].status).to eq(:debian_developer)
      expect(o['23'].gitlab_status).to eq(:active)
    end
  end

  describe '#sync_add' do
    it 'syncs known user' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:get_user).with(1)
      expect(client).to receive(:set_user_custom_attribute).with(1, 'nm-sync', 'true')

      o = SalsaNmSync::Salsa::UserList.new(
        client: client,
      )

      user = SalsaNmSync::User.new(
        sub: '1',
        email: nil,
        profile: nil,
        status: nil,
      )

      o.sync_add(user)
      expect(o['1']).not_to be_nil
    end

    it 'ignores unknown user' do
      response = Class.new { def code; '404'; end }.new
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:get_user).and_raise(HTTParty::ResponseError, response)
      expect(client).not_to receive(:set_user_custom_attribute)

      o = SalsaNmSync::Salsa::UserList.new(
        client: client,
      )

      user = SalsaNmSync::User.new(
        sub: '1',
        email: nil,
        profile: nil,
        status: nil,
      )

      o.sync_add(user)
      expect(o['1']).not_to be_nil
    end
  end
end

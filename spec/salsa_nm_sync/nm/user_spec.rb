# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Nm::User do
  describe '#initialize' do
    it 'handles nil arguments' do
      o = SalsaNmSync::Nm::User.new(
        sub: 'sub',
        email: nil,
        profile: nil,
        status: nil,
      )

      expect(o.sub).to eq('sub')
      expect(o.email).to be_nil
      expect(o.profile).to be_nil
      expect(o.status).to be_nil
    end

    it 'handles blank arguments' do
      o = SalsaNmSync::Nm::User.new(
        sub: 'sub',
        email: '',
        profile: '',
        status: '',
      )

      expect(o.sub).to eq('sub')
      expect(o.email).to be_nil
      expect(o.profile).to be_nil
      expect(o.status).to be_nil
    end
  end
end

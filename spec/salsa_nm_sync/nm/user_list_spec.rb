# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Nm::UserList do
  describe '#load_remote_json' do
    it do
      response = double(HTTParty::Response)
      expect(response).to receive(:parsed_response).and_return({
        'iss' => 'remote:',
        'aud' => 'local:',
        'exp' => 0,
        'https://nm.debian.org/claims/persons' => [
          {
            'aud' => 'local:',
            'sub' => 'test1',
          },
        ],
      })

      client = double(SalsaNmSync::Utils::NmApi)
      expect(client).to receive(:get_salsa_export).and_return(response)

      o = SalsaNmSync::Nm::UserList.new(
        audience: 'local:',
        issuer: 'remote:',
        timestamp: 0,
        client: client,
      )
      o.load_remote_json

      expect(o.size).to eq(1)
    end
  end

  describe '#load_struct' do
    it 'works with empty person list' do
      o = SalsaNmSync::Nm::UserList.new(
        audience: 'test:',
        issuer: 'test:',
        timestamp: 0,
        client: nil,
      )
      o.load_struct({
        'https://nm.debian.org/claims/persons' => [
        ],
      })

      expect(o.size).to eq(0)
    end

    it 'works with non-empty person list' do
      o = SalsaNmSync::Nm::UserList.new(
        audience: 'test:',
        issuer: 'test:',
        timestamp: 0,
        client: nil,
      )
      o.load_struct({
        'https://nm.debian.org/claims/persons' => [
          {
            'aud' => 'test:',
            'sub' => 'test1',
          },
          {
            'aud' => 'test:',
            'sub' => 'test2',
          },
        ],
      })

      expect(o.size).to eq(2)
      expect(o['test1'].sub).to eq('test1')
      expect(o['test2'].sub).to eq('test2')
    end

    it 'ignores person with unknown issuer' do
      o = SalsaNmSync::Nm::UserList.new(
        audience: 'test:',
        issuer: 'test:',
        timestamp: 0,
        client: nil,
      )
      o.load_struct({
        'https://nm.debian.org/claims/persons' => [
          {
            'aud' => 'unknown:',
            'sub' => 'test1',
          },
        ],
      })

      expect(o.size).to eq(0)
    end
  end
end

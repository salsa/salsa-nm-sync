# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::UserList do
  describe '#sync!' do
    it 'calls sync! on user' do
      from_user = double(SalsaNmSync::User)
      from = SalsaNmSync::UserList.new(users: {
        'a': from_user,
      })
      to_user = double(SalsaNmSync::User)
      to = SalsaNmSync::UserList.new(users: {
        'a': to_user,
      })
      expect(to_user).to receive(:sync!).with(from_user)
      expect(to).not_to receive(:sync_add)
      expect(to).not_to receive(:sync_remove)

      to.sync!(from)
    end

    it 'calls sync_add' do
      from = SalsaNmSync::UserList.new(users: {
        'a': 1,
      })
      to = SalsaNmSync::UserList.new
      expect(to).to receive(:sync_add).with(1)
      expect(to).not_to receive(:sync_remove)

      to.sync!(from)
    end

    it 'calls sync_remove' do
      from = SalsaNmSync::UserList.new
      to = SalsaNmSync::UserList.new(users: {
        'a': 1,
      })
      expect(to).not_to receive(:sync_add)
      expect(to).to receive(:sync_remove).with(1)

      to.sync!(from)
    end
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Utils::NmApi do
  describe '#get_salsa_export' do
    it do
      stub_request(:get, 'https://example.com/am/salsa-export/')
        .to_return(
          headers: { 'Content-Type': 'application/json' },
          body: '{"test": "test"}',
      )

      o = SalsaNmSync::Utils::NmApi.new(
        'https://example.com',
      )
      r = o.get_salsa_export

      expect(r.code).to eq(200)
      expect(r.parsed_response).to eq({'test' => 'test'})
    end
  end
end

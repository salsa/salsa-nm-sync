# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::Utils::GitlabApi do
  describe '#get' do
    it do
      stub_request(:get, 'https://example.com/api/v4/test?param=1')
        .with(headers: { 'Accept' => 'application/json', 'Authorization' => 'BEARER test' })
        .to_return(
          headers: { 'Content-Type': 'application/json' },
          body: '{"test": "test"}',
      )

      o = SalsaNmSync::Utils::GitlabApi.new(
        'https://example.com',
        'test'
      )
      r = o.get('test', query: {'param' => 1})

      expect(r.code).to eq(200)
      expect(r.parsed_response).to eq({'test' => 'test'})
    end
  end

  describe '#get_pagination_offset' do
    it do
      stub_request(:get, 'https://example.com/api/v4/test?order_by=id&param=1&per_page=1000&sort=asc')
        .with(headers: { 'Accept' => 'application/json' })
        .to_return(
          headers: { 'Content-Type': 'application/json', 'Link': '<https://example.com/api/v4/test?page=2>; rel="next", <test>; rel="last"' },
          body: '[{"id": 1}]',
      )
      stub_request(:get, 'https://example.com/api/v4/test?page=2')
        .with(headers: { 'Accept' => 'application/json' })
        .to_return(
          headers: { 'Content-Type': 'application/json' },
          body: '[{"id": 2}]',
      )

      o = SalsaNmSync::Utils::GitlabApi.new(
        'https://example.com',
        'test'
      )
      r = o.get_pagination_offset('test', query: {'param' => 1})

      expect(r.code).to eq(200)
      expect(r.parsed_response).to eq([{'id' => 1}, {'id' => 2}])
    end
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::User do
  describe '#initialize' do
    it 'works with known status' do
      o = SalsaNmSync::User.new(
        sub: 'sub',
        email: 'email',
        profile: 'profile',
        status: 'debian_developer',
      )

      expect(o.sub).to eq('sub')
      expect(o.email).to eq('email')
      expect(o.profile).to eq('profile')
      expect(o.status).to eq(:debian_developer)
    end

    it 'works with unknown status' do
      o = SalsaNmSync::User.new(
        sub: 'sub',
        email: 'email',
        profile: 'profile',
        status: 'unknown',
      )

      expect(o.status).to be(nil)
    end
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::SalsaGroup::UserList do
  describe '#load_remote' do
    it do
      response = double(HTTParty::Response)
      expect(response).to receive(:parsed_response).and_return([
        {
          'id' => 23,
          'access_level' => 40,
        },
      ])

      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:get_group_members).with('debian').and_return(response)

      o = SalsaNmSync::SalsaGroup::UserList.new(
        client: client,
      )
      o.load_remote()

      expect(o.size).to eq(1)
      expect(o['23'].email).to eq(nil)
      expect(o['23'].profile).to eq(nil)
      expect(o['23'].status).to eq(:debian_developer)
    end

    it 'ignores user with access_level != 40' do
      response = double(HTTParty::Response)
      expect(response).to receive(:parsed_response).and_return([
        {
          'id' => 23,
          'access_level' => 50,
        },
      ])

      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:get_group_members).with('debian').and_return(response)

      o = SalsaNmSync::SalsaGroup::UserList.new(
        client: client,
      )
      o.load_remote()

      expect(o.size).to eq(0)
    end
  end

  describe '#sync_add' do
    it 'adds user with status == debian_developer' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:add_group_member).with('debian', 1)

      o = SalsaNmSync::SalsaGroup::UserList.new(
        client: client,
      )

      user = SalsaNmSync::User.new(
        sub: '1',
        email: nil,
        profile: nil,
        status: 'debian_developer',
      )

      o.sync_add(user)
      expect(o['1']).not_to be_nil
    end

    it 'ignores user with status != debian_developer' do
      client = double(SalsaNmSync::Utils::GitlabApi)

      o = SalsaNmSync::SalsaGroup::UserList.new(
        client: client,
      )

      user = SalsaNmSync::User.new(
        sub: '1',
        email: nil,
        profile: nil,
        status: nil,
      )

      o.sync_add(user)
      expect(o['1']).to be_nil
    end
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'spec_helper'

describe SalsaNmSync::SalsaGroup::User do
  describe '#initialize' do
    it do
      o = SalsaNmSync::SalsaGroup::User.new(
        id: 1,
        status: 'debian_developer',
        client: nil,
      )

      expect(o.sub).to eq('1')
      expect(o.email).to eq(nil)
      expect(o.profile).to eq(nil)
      expect(o.status).to eq(:debian_developer)
    end
  end

  describe '#status=' do
    it 'no change if new status == old status' do
      client = double(SalsaNmSync::Utils::GitlabApi)

      user = SalsaNmSync::SalsaGroup::User.new(
        id: 1,
        status: 'debian_developer',
        client: client,
      )

      user.status = :debian_developer
    end

    it 'adds member if new status == debian_developer' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:add_group_member).with('debian', 1)

      user = SalsaNmSync::SalsaGroup::User.new(
        id: 1,
        status: nil,
        client: client,
      )

      user.status = :debian_developer
    end

    it 'removes member if old status == debian_developer' do
      client = double(SalsaNmSync::Utils::GitlabApi)
      expect(client).to receive(:remove_group_member).with('debian', 1)

      user = SalsaNmSync::SalsaGroup::User.new(
        id: 1,
        status: 'debian_developer',
        client: client,
      )

      user.status = nil
    end
  end
end

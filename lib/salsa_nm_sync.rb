# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'zeitwerk'

loader = Zeitwerk::Loader.for_gem
loader.setup

module SalsaNmSync
  class << self
    attr_accessor :logger
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::Nm::UserList < SalsaNmSync::UserList
  CLAIM_NM_DEBIAN_STATUS = 'https://nm.debian.org/claims/debian_status'
  CLAIM_NM_PERSONS = 'https://nm.debian.org/claims/persons'

  def initialize(audience:, issuer:, timestamp:, client:)
    super()
    @audience = audience
    @issuer = issuer
    @timestamp = timestamp
    @client = client
  end

  def load_remote_json
    load_struct @client.get_salsa_export.parsed_response
  end

  def load_struct(struct)
    struct.fetch(CLAIM_NM_PERSONS, []).each do |person|
      if person.fetch('aud') == @audience
        sub = person.fetch('sub')
        email = person['email']
        profile = person['profile']
        status = person[CLAIM_NM_DEBIAN_STATUS]

        @users[sub] = SalsaNmSync::Nm::User.new(
          sub: sub,
          email: email,
          profile: profile,
          status: status,
        )
      end
    end
  end
end

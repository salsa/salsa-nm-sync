# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::Nm::User < SalsaNmSync::User
  def initialize(sub:, email:, profile:, status:)
    super(
      sub: sub,
      email: email == '' ? nil : email,
      profile: profile == '' ? nil : profile,
      status: status,
    )
  end
end

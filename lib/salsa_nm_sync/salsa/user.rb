# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::Salsa::User < SalsaNmSync::User
  class GitlabStatus
    include Ruby::Enum

    define :active, 'active'
    define :blocked, 'blocked'
  end

  ATTRIBUTE_NM_EMAIL = 'nm-sync-email'
  ATTRIBUTE_NM_PROFILE = 'nm-sync-profile'
  ATTRIBUTE_NM_STATUS = 'nm-sync-status'

  attr_reader :id, :gitlab_status

  def initialize(id:, custom_attributes:, status:, client:)
    super(
      sub: id.to_s,
      email: custom_attributes[ATTRIBUTE_NM_EMAIL],
      profile: custom_attributes[ATTRIBUTE_NM_PROFILE],
      status: custom_attributes[ATTRIBUTE_NM_STATUS],
    )
    @id = id
    @custom_attributes = custom_attributes
    @gitlab_status = GitlabStatus.key(status)
    @client = client
  end

  def sync!(from)
    self.email = from.email
    self.profile = from.profile
    self.status = from.status
  end

  def email=(from)
    if @email != from
      set_custom_attribute(ATTRIBUTE_NM_EMAIL, from)
      @email = from
    end
  end

  def profile=(from)
    if @profile != from
      set_custom_attribute(ATTRIBUTE_NM_PROFILE, from)
      @profile = from
    end
  end

  def status=(from)
    if @status != from
      set_custom_attribute(ATTRIBUTE_NM_STATUS, from)
      @status = from
    end

    if @status == :debian_removed_dd
      self.gitlab_status = :blocked
    end
  end

  def gitlab_status=(from)
    if @gitlab_status != from
      SalsaNmSync.logger&.warn("Update status of user #{@id} from #{@gitlab_status.inspect} to #{from.inspect}")
      case from
      when :blocked
        @client.block_user(@id)
      end
      @gitlab_status = from
    end
  end

  def set_custom_attribute(key, value)
    SalsaNmSync.logger&.warn("Update custom attribute #{key} of user #{@id} from #{@custom_attributes[key].inspect} to #{value.inspect}")
    if value.nil?
      @client.delete_user_custom_attribute(@id, key)
    else
      @client.set_user_custom_attribute(@id, key, value.to_s)
    end
  end
end

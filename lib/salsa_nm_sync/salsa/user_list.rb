# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::Salsa::UserList < SalsaNmSync::UserList
  ATTRIBUTE_SALSA_NM_SYNC = 'nm-sync'

  def initialize(client:)
    super()
    @client = client
  end

  def load_remote
    r = @client.get_users query: {
      with_custom_attributes: true,
      "custom_attributes[#{ATTRIBUTE_SALSA_NM_SYNC}]": true,
    }
    load_struct r.parsed_response
  end

  def load_struct(struct)
    struct.each do |user|
      id = user.fetch('id')
      custom_attributes = user.fetch('custom_attributes').map do |i|
        [i['key'], i['value']]
      end.to_h

      @users[id.to_s] = SalsaNmSync::Salsa::User.new(
        id: id,
        custom_attributes: custom_attributes,
        status: user.fetch('state'),
        client: @client,
      )
    end
  end

  def sync_add(user)
    SalsaNmSync.logger&.warn("Adopt Salsa user #{user.sub} (#{user.profile})")
    @users[user.sub] = n = SalsaNmSync::Salsa::User.new(
      id: Integer(user.sub),
      custom_attributes: {},
      status: :active,
      client: @client,
    )
    begin
      @client.get_user(n.id)
    rescue HTTParty::ResponseError => e
      # XXX: gitlab fails with exception currently during custom attribute calls
      if e.response.code == '404'
        SalsaNmSync.logger&.error("User #{user.sub} does not exist")
      else
        raise
      end
    else
      n.set_custom_attribute(ATTRIBUTE_SALSA_NM_SYNC, true)
      n.sync!(user)
    end
  end

  def sync_remove(user)
    SalsaNmSync.logger&.warn("Relinquish Salsa user #{user.sub} (#{user.profile}) (not implemented)")
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::SalsaGroup::UserList < SalsaNmSync::UserList
  def initialize(client:)
    super()
    @client = client
  end

  def load_remote
    r = @client.get_group_members('debian')
    load_struct r.parsed_response
  end

  def load_struct(struct)
    struct.each do |user|
      if user.fetch('access_level') == 40
        id = user.fetch('id')

        @users[id.to_s] = SalsaNmSync::SalsaGroup::User.new(
          id: id,
          status: 'debian_developer',
          client: @client,
        )
      end
    end
  end

  def sync_add(user)
    return if user.status != :debian_developer

    @users[user.sub] = n = SalsaNmSync::SalsaGroup::User.new(
      id: Integer(user.sub),
      status: nil,
      client: @client,
    )
    n.sync!(user)
  end

  def sync_remove(user)
    user.status = nil
  end
end

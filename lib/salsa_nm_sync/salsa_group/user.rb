# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

class SalsaNmSync::SalsaGroup::User < SalsaNmSync::User
  def initialize(id:, status:, client:)
    super(
      sub: id.to_s,
      email: nil,
      profile: nil,
      status: status,
    )
    @id = id
    @client = client
  end

  def sync!(from)
    self.status = from.status
  end

  def status=(from)
    begin
      if @status == :debian_developer and from != :debian_developer
        SalsaNmSync.logger&.warn("Remove user #{@id} from group debian")
        @client.remove_group_member('debian', @id)
      elsif @status != :debian_developer and from == :debian_developer
        SalsaNmSync.logger&.warn("Add user #{@id} to group debian")
        @client.add_group_member('debian', @id)
      end
    rescue HTTParty::ResponseError => e
      if e.response.code == '404'
        SalsaNmSync.logger&.error("User #{@id} does not exist")
      else
        raise
      end
    end
  end
end

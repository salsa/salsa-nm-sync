# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'ruby-enum'

class SalsaNmSync::User
  class DebianStatus
    include Ruby::Enum

    define :debian_developer, 'debian_developer'
    define :debian_maintainer, 'debian_maintainer'
    define :debian_emeritus, 'debian_emeritus'
    define :debian_removed_dd, 'debian_removed_dd'
  end

  attr_reader :sub, :email, :profile, :status

  def initialize(sub:, email:, profile:, status:)
    @sub = sub
    @email = email
    @profile = profile
    @status = DebianStatus.key(status)
  end
end

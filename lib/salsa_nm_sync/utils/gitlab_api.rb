# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'httparty'

class SalsaNmSync::Utils::GitlabApi
  DEFAULT_PAGE_SIZE = 1000

  include HTTParty
  follow_redirects false
  headers 'Accept' => 'application/json'
  raise_on 300..599

  def initialize(url, token, noop: false)
    @noop = noop

    @url = URI::join(url, '/api/v4/')
    if @url.scheme != 'https'
      raise RuntimeError.new 'Require https scheme'
    end

    @headers = {
      authorization: "BEARER #{token}",
    }
  end

  def delete(resource, query: nil)
    url = URI::join(@url, resource)
    if !@noop
      self.class.delete(
        URI::join(@url, resource),
        headers: @headers,
        logger: SalsaNmSync.logger,
        query: query,
      )
    else
      SalsaNmSync.logger&.info("Would have done \"DELETE #{url}\"")
    end
  end

  def get(resource, query: nil)
    url = URI::join(@url, resource)
    self.class.get(
      url,
      headers: @headers,
      logger: SalsaNmSync.logger,
      query: query,
    )
  end

  def post(resource, body:, query: nil)
    url = URI::join(@url, resource)
    if !@noop
      self.class.post(
        URI::join(@url, resource),
        headers: @headers,
        logger: SalsaNmSync.logger,
        query: query,
        body: body,
      )
    else
      SalsaNmSync.logger&.info("Would have done \"POST #{url}\"")
    end
  end

  def put(resource, body:, query: nil)
    url = URI::join(@url, resource)
    if !@noop
      self.class.put(
        url,
        headers: @headers,
        logger: SalsaNmSync.logger,
        query: query,
        body: body,
      )
    else
      SalsaNmSync.logger&.info("Would have done \"PUT #{url}\"")
    end
  end

  def get_pagination_offset(resource, query: nil)
    ret = []
    page = 0
    base_query = (query || {}).merge({
      per_page: DEFAULT_PAGE_SIZE,
      order_by: 'id',
      sort: 'asc',
    })
    url = URI::join(@url, resource)
    url.query = HTTParty::HashConversions.to_params(base_query)

    while url
      r = self.class.get(
        url,
        headers: @headers,
        logger: SalsaNmSync.logger,
      )
      url = parse_link_header(r)[:next]
      ret += r.parsed_response
    end

    HTTParty::Response.new(HTTParty::Request.new('GET', ''), r, lambda { ret }, body: nil)
  end

  def add_group_member(id, user_id, access_level: 40)
    body = {
      access_level: access_level,
      user_id: user_id,
    }
    post("groups/#{id}/members", body: body)
  end

  def block_user(id)
    post("users/#{id}/block", body: nil)
  end

  def delete_user_custom_attribute(id, key)
    delete("users/#{id}/custom_attributes/#{key}")
  end

  def get_group_members(id, query: nil)
    get_pagination_offset("groups/#{id}/members", query: query)
  end

  def get_user(id, query: nil)
    get("users/#{id}", query: query)
  end

  def get_users(query: nil)
    get_pagination_offset('users', query: query)
  end

  def remove_group_member(id, user_id)
    delete("groups/#{id}/members/#{user_id}")
  end

  def set_user_custom_attribute(id, key, value)
    put("users/#{id}/custom_attributes/#{key}", body: { value: value })
  end

  def parse_link_header(response)
    parts = response.headers.fetch('link', '').split(',')

    parts.map do |part, _|
      section = part.split(';')
      name = section[1][/rel="(.*)"/, 1].to_sym
      url = section[0][/<(.*)>/, 1]

      [name, url]
    end.to_h
  end
end

# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'httparty'

class SalsaNmSync::Utils::NmApi
  include HTTParty
  follow_redirects false
  raise_on 300..599

  def initialize(url)
    @url = URI(url)
    if @url.scheme != 'https'
      raise RuntimeError.new 'Require https scheme'
    end
  end

  def get(resource, query: nil)
    url = URI::join(@url, resource)
    self.class.get(
      url,
      headers: @headers,
      logger: SalsaNmSync.logger,
      query: query,
    )
  end

  def get_salsa_export(query: nil)
    get('am/salsa-export/')
  end
end

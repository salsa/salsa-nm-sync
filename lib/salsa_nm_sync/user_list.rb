# frozen_string_literal: true
# SPDX-License-Identifier: GPL-3.0-or-later

require 'forwardable'

class SalsaNmSync::UserList
  extend Forwardable

  def_delegators :@users, :[], :each, :keys, :size

  def initialize(users: {})
    @users = users
  end

  def sync!(from)
    from_keys = Set.new(from.keys)
    to_keys = Set.new(keys)

    (from_keys - to_keys).each do |key|
      sync_add(from[key])
    end

    (from_keys & to_keys).each do |key|
      self[key].sync!(from[key])
    end

    (to_keys - from_keys).each do |key|
      sync_remove(self[key])
    end
  end

  def sync_add(user)
    raise NotImplenmentedError
  end

  def sync_remove(user)
    raise NotImplenmentedError
  end
end
